<?php

namespace Drupal\workout\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class WorkoutCommands extends DrushCommands {

  /**
   * Runs a workout on on all nodes.
   *
   * @param int $nid
   *   Providing a node id will restrict the run to a single node.
   * @param array $options
   *   Command Options.
   *
   * @option host
   *   Setting the --host option runs the tests on a different system.
   * @option curl
   *   Setting the --curl option uses curl to access your local system.
   * @option curl_timeout
   *   Curl timeout in seconds.
   * @option db
   *   Debug.
   * @usage drush workout
   *   - Runs a workout on all nodes.
   *
   * @command workout:workout
   * @aliases workout wk
   */
  public function workout($nid = NULL, array $options = [
    'host' => NULL,
    'curl' => FALSE,
    'curl_timeout' => 2,
    'db' => FALSE,
  ]) {

    global $base_url;
    $host = (isset($options['host']) ? $options['host'] : '');
    if (str_contains($base_url, '//default') && empty($host)) {
      print "The base_url global variable is not set for this web site.\r\n";
      print "It is set to the default url: $base_url.\r\n";
      print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
      print "Add something like this to your drush commands:\r\n";
      print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
      print "\r\n";
      print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
      print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
      return;
    }

    $run_timestamp = time();

    \Drupal::logger('marker')->notice('MARKER - Start Workout Run - ' . date("Y/m/d H:i:s", $run_timestamp));
    $host = (isset($options['host']) ? $options['host'] : $base_url);
    $curl = $options['curl'];
    $curl_timeout = (isset($options['curl_timeout']) ? $options['curl_timeout'] : 2);
    $db = $options['db'];

    if ($nid == NULL) {
      $this->emitDividerLine();
      $this->emitHeaderLine();
      $this->emitDividerLine();

      $node_count = 1;
      $connection = \Drupal::database();
      $query = $connection->query("SELECT `nid`, `type` FROM {node_field_data} WHERE `status` = 1 ORDER BY `nid` ASC");
      $results = $query->fetchAll();
      foreach ($results as $row) {
        $fields_array = workout_test_node($row->nid, $row->type, $host, $curl, $curl_timeout, $db, $run_timestamp);
        $fields_array['id'] = $node_count;
        $this->emitDataLine($fields_array);
        $node_count++;
      }
      $this->emitDividerLine();

    }

    $this->logger()->success(dt('Completed - drush workout. ' . $nid));
    print "\r\n\r\nCompleted - drush workout $nid.\r\n\r\n";
    \Drupal::logger('marker')->notice('MARKER - End Workout Run - ' . date("Y/m/d H:i:s", $run_timestamp));

  }

  /**
   * Restarts a workout run on all nodes.
   *
   * @param int $nid
   *   Providing a node id restarts at that point. Leave blank for auto restart.
   * @param array $options
   *   Command Options.
   *
   * @option host
   *   Setting the --host option runs the tests on a different system.
   * @option curl
   *   Setting the --curl option uses curl to access your local system.
   * @option curl_timeout
   *   Curl timeout in seconds.
   * @option db
   *   Debug.
   * @usage drush workout-restart
   *   - Restarts a workout run on all nodes.
   *
   * @command workout:workoutReStart
   * @aliases workout-restart wkrs
   */
  public function workoutReStart($nid = NULL, array $options = [
    'host' => NULL,
    'curl' => FALSE,
    'curl_timeout' => 2,
    'db' => FALSE,
  ]) {

    global $base_url;
    $host = (isset($options['host']) ? $options['host'] : '');
    if (str_contains($base_url, '//default') && empty($host)) {
      print "The base_url global variable is not set for this web site.\r\n";
      print "It is set to the default url: $base_url.\r\n";
      print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
      print "Add something like this to your drush commands:\r\n";
      print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
      print "\r\n";
      print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
      print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
      return;
    }

    $run_timestamp = $this->chooseRun('Run To Restart');
    if ($run_timestamp === FALSE) {
      print "\r\nRestart Canceled\r\n";
      return;
    }

    $host = (isset($options['host']) ? $options['host'] : $base_url);
    $curl = $options['curl'];
    $curl_timeout = (isset($options['curl_timeout']) ? $options['curl_timeout'] : 2);
    $db = $options['db'];

    if ($run_timestamp == 0) {
      print "\r\n\r\n Canceled - drush workout-restart $nid.\r\n\r\n";
      return;
    }

    \Drupal::logger('workout')->notice('Restart Run.');
    $connection = \Drupal::database();

    if ($nid == NULL) {
      // Find the max(nid) for the run and start there.
      $nid = $connection->query("SELECT MAX(`nid`) FROM {workout} WHERE `run` = :run",
                                [
                                  ':run' => $run_timestamp,

                                ])->fetchField();

    }

    if (is_numeric($nid)) {
      $this->emitDividerLine();
      $this->emitHeaderLine();
      $this->emitDividerLine();

      $query = $connection->query("SELECT `nid`, `type` FROM {node_field_data}  WHERE  `status` = 1 AND `nid` >= :nid ORDER BY `nid` ASC",
                                 [
                                   ':nid' => $nid,
                                 ]);
      $results = $query->fetchAll();
      foreach ($results as $row) {

        $this->emitDataLine(workout_test_node($row->nid, $row->type, $host, $curl, $curl_timeout, $db, $run_timestamp));
      }

      $this->emitDividerLine();

    }

    print "\r\n\r\n Completed - drush workout-restart $nid.\r\n\r\n";
    \Drupal::logger('marker')->notice('MARKER - End Workout Run - ' . date("Y/m/d H:i:s", $run_timestamp));

    $this->logger()->success(dt('workout  restart command function. ' . $arg1));
  }

  /**
   * Reports on previous workouts.
   *
   * @param array $options
   *   Command line options.
   *
   * @option host
   *   Description
   * @usage drush workout-report
   *   Reports on previous workouts.
   *
   * @command workout:workoutReport
   * @aliases workout-report wkr
   */
  public function workoutReport(array $options = [
    'all' => FALSE,
    'csv' => FALSE,
  ]) {

    // If csv report requeted.
    if ($options['csc']) {
      print workout_get_csv_report();
      return;
    }

    $node_count = 0;
    $display_count = 0;
    $hide_node = TRUE;
    // Table Header.
    $this->emitDividerLine();
    $this->emitHeaderLine();

    $buffer = '';
    $row_nid = 0;
    $connection = \Drupal::database();
    $query = $connection->query("SELECT * FROM {workout} ORDER BY `nid` ASC, `run` ASC");
    $results = $query->fetchAll();
    foreach ($results as $row) {

      // If this is a different node.
      if ($row->nid != $row_nid) {
        $row_nid = $row->nid;
        if ($hide_node == FALSE || $options['all']) {
          $this->emitDividerLine();
          print $buffer;
          $display_count++;
        }

        $buffer = '';
        $hide_node = TRUE;
        $node_count++;
      }

      // Text output.
      $buffer .= '| ' .
      str_pad($row->id, 6) . '| ' .
      str_pad(date('Y/m/d H:i:s', $row->run), 20) . '| ' .
      str_pad($row->nid, 15) . '| ' .
      str_pad(substr($row->type, 0, 14), 15) . '| ' .
      str_pad($row->html_status, 6) . '| ' .
      str_pad($row->content_size, 8) . '| ' .
      str_pad($row->wid_before_test, 12) . '| ' .
      str_pad($row->wid_after_test, 12) . '| ' .
      str_pad($row->delta_watchdog, 6) . '| ' .
      str_pad($row->delta_php, 6) . '| ' .
      str_pad($row->delta_emergency, 6) . '| ' .
      str_pad($row->delta_alert, 6) . '| ' .
      str_pad($row->delta_critical, 6) . '| ' .
      str_pad($row->delta_error, 6) . '| ' .
      str_pad($row->delta_warning, 6) . '| ' .
      str_pad($row->delta_notice, 6) . '| ' .
      str_pad($row->delta_info, 6) . '| ' .
      str_pad($row->delta_debug, 6) . '| ' .
      str_pad(date('Y/m/d H:i:s', $row->timestamp), 20) . "|\r\n";

      if ($row->wid_before_test != $row->wid_after_test) {
        $hide_node = FALSE;
      }

    }

    // Table Footer.
    $this->emitDividerLine();

    if ($display_count == $node_count) {
      print "\r\nUnfiltered  - $node_count Nodes.\r\n";
    }
    else {
      print "\r\nFiltered  - $display_count Of $node_count Nodes.\r\n";
      print "\r\nFor an Unfiltered report add `--all` to the drush command.\r\n";
    }

    $this->logger()->success(dt('Completed - workout-report.'));
  }

  /**
   * Runs a workout on a specified node.
   *
   * @param int $arg
   *   Providing the node id of the node to be tested.
   * @param array $options
   *   Command Options.
   *
   * @option host
   *   Description
   * @usage drush workout-test
   *   Runs a workout on a specified node.
   *
   * @command workout:workoutTest
   * @aliases workout-test wkt
   */
  public function workoutTest($arg, array $options = [
    'host' => NULL,
    'curl' => FALSE,
    'curl_timeout' => 2,
    'db' => FALSE,
  ]) {

    global $base_url;
    $host = (isset($options['host']) ? $options['host'] : '');
    if (str_contains($base_url, '//default') && empty($host)) {
      print "The base_url global variable is not set for this web site.\r\n";
      print "It is set to the default url: $base_url.\r\n";
      print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
      print "Add something like this to your drush commands:\r\n";
      print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
      print "\r\n";
      print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
      print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
      return;
    }

    if ($arg == NULL) {
      print "\r\n\r\n You must specify either a url or node id to test.\r\n\r\n";
    }
    elseif (is_numeric($arg)) {
      $host = (isset($options['host']) ? $options['host'] : $base_url);
      $curl = $options['curl'];
      $curl_timeout = (isset($options['curl_timeout']) ? $options['curl_timeout'] : 2);
      $db = $options['db'];
      $this->emitDividerLine();
      $this->emitHeaderLine();
      $this->emitDividerLine();
      $this->emitDataLine(workout_test_node($arg, 'type_unknown', $host, $curl, $curl_timeout, $db, time()));
      $this->emitDividerLine();

      print "\r\n\r\n Completed - drush workout-test $nid. \r\n\r\n";
    }
    elseif (!empty($arg)) {
      $content_length = 0;
      $html_status_code = 0;

      if ($curl) {
        workout_curl_get_contents($arg, $content_length, $html_status_code);
      }
      else {
        workout_file_get_contents($arg, $content_length, $html_status_code);
      }

      print "\r\n\r\n";
      $this->emitDividerLine();
      print "| URL:\t\t\t $arg\r\n";
      print "| CONTENT_LENGTH\t $content_length\r\n";
      print "| HTML_STATUS\t\t $html_status_code\r\n";
      $this->emitDividerLine();

      print "\r\nCompleted - workout-test $arg.\r\n\r\n";
    }

    $this->logger()->success(dt('workout test command function. '));
  }

  /**
   * List and Manage previous run results.
   *
   * @param array $options
   *   Command  line options.
   *
   * @option all
   *   Setting the --host option runs the tests on a different system.
   * @usage drush workout-history
   *   List and Manage previous run results.
   *
   * @command workout:workoutHistory
   * @aliases workout-history wkh
   */
  public function workoutHistory(array $options = [
    'all' => FALSE,
  ]) {

    if ($options['all']) {

      if (drush_confirm('Are you sure you want to delete all history ?')) {
        \Drupal::database()->truncate('workout')->execute();
      }
      else {
        print "\r\n\r\n Canceled - drush workout history..\r\n\r\n";
        return;
      }

      print "\r\n\r\n Completed - drush workout history.\r\n\r\n";
    }
    else {
      // Choose a Run to delete.
      $run_timestamp = $this->chooseRun('To Delete');

      $connection = \Drupal::database();
      $query = $connection->query("DELETE FROM {workout} WHERE `run` = :run",
        [
          ':run' => $run_timestamp,

        ]);
    }

    $this->logger()->success(dt('workout history command function. '));
  }

  /**
   * Gets the $base_url of thia drupal site this includes the schema.
   *
   * @usage drush get-base-url
   *   - Gets the base url of this web site.
   *
   * @command workout:getBaseUrl
   * @aliases get-base-url gbu
   */
  public function getBaseUrl() {
    global $base_url;
    print "\r\n$base_url\r\n\r\n";
    if (str_contains($base_url, '//default')) {
      print "The base_url global variable is not set for this web site.\r\n";
      print "It is set to the default url: $base_url.\r\n";
      print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
      print "Add something like this to your drush commands:\r\n";
      print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
      print "\r\n";
      print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
      print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
    }

  }

  /*
   * Helpers.
   */

  /**
   * Choose A Run.
   */
  public function chooseRun($why) {
    global $_grouper_count;
    $cancel = [0 => 'CANCEL'];
    $connection = \Drupal::database();
    $query = $connection->query("SELECT DISTINCT `run` FROM {workout} ORDER BY `run` ASC");
    $results = $query->fetchAll();

    $choices = array_merge($cancel, $results);

    print "\r\n RUNS\r\n\r\n";
    foreach ($choices as $key => $row) {
      if ($key == 0) {
        print ' ' . str_pad((string) $key, 3) . ' -- ' . str_pad('CANCEL', 32) . "\r\n";

      }
      else {
        print ' ' . str_pad((string) $key, 3) . ' -- ' . str_pad(date('Y/m/d H:i:s', $row->run), 32) . "\r\n";
      }
    }

    $_grouper_count = count($choices);

    $choice = $this->io()->ask($why, 1, function ($number) {
      global $_grouper_count;

      if (!is_numeric($number)) {
        throw new \RuntimeException('You must type a number.');
      }

      elseif ($number < 0 || $number > $_grouper_count - 1) {
        throw new \RuntimeException('Invalid Choice.');
      }

      return (int) $number;
    });

    if ($choice == 0) {
      return FALSE;
    }

    return $choices[$choice]->run;

  }

  /**
   * Displays Divider Line.
   */
  public function emitDividerLine() {
    print '+' . str_pad('-', 211, '-') . "+\r\n";
  }

  /**
   * Displays Header Line.
   */
  public function emitHeaderLine() {

    // TEXT output.
    print '| ' .
      str_pad('Id', 6) . '| ' .
      str_pad('Run Date', 20) . '| ' .
      str_pad('Nid', 15) . '| ' .
      str_pad('Content Type', 15) . '| ' .
      str_pad('Status', 6) . '| ' .
      str_pad('Size', 8) . '| ' .
      str_pad('Wid Before', 12) . '| ' .
      str_pad('Wid After', 12) . '| ' .
      str_pad('∆ wid ', 6) . '| ' .
      str_pad('∆ php ', 6) . '| ' .
      str_pad('∆mrgnc', 6) . '| ' .
      str_pad('∆alert', 6) . '| ' .
      str_pad('∆crtcl', 6) . '| ' .
      str_pad('∆error', 6) . '| ' .
      str_pad('∆warn ', 6) . '| ' .
      str_pad('∆notic', 6) . '| ' .
      str_pad('∆ info', 6) . '| ' .
      str_pad('∆debug', 6) . '| ' .
      str_pad('Time Stamp', 20) . "|\r\n";

  }

  /**
   * Displays Data Line.
   */
  public function emitDataLine($field_array) {

    print '| ' .
      str_pad($field_array['id'], 6) . '| ' .
      str_pad(date('Y/m/d H:i:s', $field_array['run']), 20) . '| ' .
      str_pad($field_array['nid'], 15) . '| ' .
      str_pad(substr($row->type, 0, 14), 15) . '| ' .
      str_pad($field_array['html_status'], 6) . '| ' .
      str_pad($field_array['content_size'], 8) . '| ' .
      str_pad($field_array['wid_before_test'], 12) . '| ' .
      str_pad($field_array['wid_after_test'], 12) . '| ' .
      str_pad($field_array['delta_watchdog'], 6) . '| ' .
      str_pad($field_array['delta_php'], 6) . '| ' .
      str_pad($field_array['delta_emergency'], 6) . '| ' .
      str_pad($field_array['delta_alert'], 6) . '| ' .
      str_pad($field_array['delta_critical'], 6) . '| ' .
      str_pad($field_array['delta_error'], 6) . '| ' .
      str_pad($field_array['delta_warning'], 6) . '| ' .
      str_pad($field_array['delta_notice'], 6) . '| ' .
      str_pad($field_array['delta_info'], 6) . '| ' .
      str_pad($field_array['delta_debug'], 6) . '| ' .
      str_pad(date('Y/m/d H:i:s', $field_array['timestamp']), 20) . "|\r\n";

  }

}
