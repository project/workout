<?php

namespace Drupal\workout\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for page example routes.
 */
class WorkoutController extends ControllerBase {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'workout';
  }

  /**
   * Constructs a simple page.
   *
   * The router _controller callback, maps the path
   * 'examples/page-example/simple' to this method.
   *
   * _controller callbacks return a renderable array for the content area of the
   * page. The theme system will later render and surround the content with the
   * appropriate blocks, navigation, and styling.
   */
  public function simple() {

    $form['table'] = [
      '#type' => 'table',
      '#header' => [1, 2, 3, 4],
      '#rows' => [1, 2, 3, 4],
    ];
    return $form;

  }

  /**
   * Summary report page showing only nodes with errors.
   */
  public function summary() {
    $last_nid = 0;
    $row_class = 'nid-clear';
    $temp_rows = [];
    $rows = [];
    $hide_node = TRUE;

    $query = "SELECT * FROM {workout} ORDER BY `nid` ASC, `run` ASC";
    $result = $this->database->query($query);

    $header = [
      'Date',
      'Run Date',
      'Nid',
      'Content Type',
      'Status',
      'Size',
      'Wid Before',
      'Wid After',
      '∆ wid ',
      '∆ php ',
      '∆mrgnc',
      '∆alert',
      '∆crtcl',
      '∆error',
      '∆warn ',
      '∆notic',
      '∆ info',
      '∆debug',
    ];

    foreach ($result as $record) {

      // If the nid has changed.
      if ($last_nid != $record->nid) {

        if ($hide_node == FALSE) {
          // If we have NOT been hiding it.
          $rows = array_merge($rows, $temp_rows);

          $hide_node = TRUE;

          $row_class = ($row_class == 'nid-clear' ? 'nid-shaded' : 'nid-clear');
        }

        $temp_rows = [];
        $last_nid = $record->nid;

      }

      $temp_rows[] = [
        'data' => [
          date('Y/m/d H:i:s', $record->timestamp),
          date('Y/m/d H:i:s', $record->run),
          $this->getWorkoutNodeLink($record->nid),
          $record->type,
          $record->html_status,
          $record->content_size,
          $record->wid_before_test,
          $this->getWorkoutWidLink($record->wid_before_test, $record->wid_after_test),
          $record->delta_watchdog,
          $record->delta_php,
          $record->delta_emergency,
          $record->delta_alert,
          $record->delta_critical,
          $record->delta_error,
          $record->delta_warning,
          $record->delta_notice,
          $record->delta_info,
          $record->delta_debug,
        ],
        'class' => [$row_class],
      ];

      if ($record->wid_before_test != $record->wid_after_test) {
        $hide_node = FALSE;
      }

    }

    $form['workout_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['id' => 'admin-workout-summary', 'class' => ['admin-workout-summary']],
      '#empty' => $this->t('No Data available.'),
      '#attached' => [
        'library' => ['workout/workout_summary'],
      ],
    ];

    $form['workout_pager'] = ['#type' => 'pager'];

    return $form;

  }

  /**
   * Summary report page showing all nodes with or without errors.
   */
  public function summaryFull() {
    $last_nid = 0;
    $row_class = 'nid-clear';

    $query = "SELECT * FROM {workout} ORDER BY `nid` ASC, `run` ASC";
    $result = $this->database->query($query);

    $header = [
      'Date',
      'Run Date',
      'Nid',
      'Content Type',
      'Status',
      'Size',
      'Wid Before',
      'Wid After',
      '∆ wid ',
      '∆ php ',
      '∆mrgnc',
      '∆alert',
      '∆crtcl',
      '∆error',
      '∆warn ',
      '∆notic',
      '∆ info',
      '∆debug',
    ];

    $rows = [];

    foreach ($result as $record) {

      if ($last_nid != $record->nid) {
        // The nid changed.
        $row_class = ($row_class == 'nid-clear' ? 'nid-shaded' : 'nid-clear');
        $last_nid = $record->nid;
      }

      $rows[] = [
        'data' => [
          date('Y/m/d H:i:s', $record->timestamp),
          date('Y/m/d H:i:s', $record->run),
          $this->getWorkoutNodeLink($record->nid),
          $record->type,
          $record->html_status,
          $record->content_size,
          $record->wid_before_test,
          $this->getWorkoutWidLink($record->wid_before_test, $record->wid_after_test),
          $record->delta_watchdog,
          $record->delta_php,
          $record->delta_emergency,
          $record->delta_alert,
          $record->delta_critical,
          $record->delta_error,
          $record->delta_warning,
          $record->delta_notice,
          $record->delta_info,
          $record->delta_debug,
        ],
        'class' => [$row_class],
      ];

    }

    $form['workout_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['id' => 'admin-workout-summary', 'class' => ['admin-workout-summary']],
      '#empty' => $this->t('No Data available.'),
      '#attached' => [
        'library' => ['workout/workout_summary'],
      ],
    ];

    $form['workout_pager'] = ['#type' => 'pager'];

    return $form;

  }

  /**
   * CSV Summary report page showing all nodes with or without errors.
   */
  public function summaryCsv() {
    $response = new Response();
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment; filename="workout-report.csv"');
    $response->setContent(workout_get_csv_report());
    return $response;
  }

  /*
   * Helpers.
   */

  /**
   * Get Workout ID Link.
   *
   * Opens related events.
   */
  private function getWorkoutIdLink($timestamp, $wid_before_test, $wid_after_test) {
    if ($wid_after_test > $wid_before_test) {
      $markup = "<a href=\"#\" class=\"id-link\" wid-before=\"$wid_before_test\" wid-after=\"$wid_after_test\">" . date('Y/m/d H:i:s', $timestamp) . '</a>';
    }
    else {
      $markup = date('Y/m/d H:i:s', $timestamp);
    }
    return ['data' => ['#markup' => $markup]];
  }

  /**
   * Get Workout node Link.
   *
   * Opens related events.
   */
  private function getWorkoutNodeLink($nid) {
    $link = "<a href=\"/node/$nid\" class=\"nid-link\" target=\"_blank\">$nid</a>";
    return ['data' => ['#markup' => $link]];
  }

  /**
   * Get Workout dblog Link.
   *
   * Opens related events.
   */
  private function getWorkoutWidLink($wid_before_test, $wid_after_test) {
    if ($wid_after_test > $wid_before_test) {
      $markup = "<a href=\"/admin/reports/dblog/event/$wid_after_test\" class=\"wid-link\" target=\"_blank\">$wid_after_test</a>";
    }
    else {
      $markup = $wid_after_test;
    }
    return ['data' => ['#markup' => $markup]];
  }

}
