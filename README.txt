CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Operation
 * Troubleshooting
 * FAQ
 * Maintainers
 * Appendix -  Drush Commands



INTRODUCTION
------------
Some php errors occur every time a page is loaded and renderded.
Exerciser can be useful for forcing these errors to be listed in Drupals Log.
Once they are listed in Drupals Log you can begin to address and repair the underlying issues. A tool like Grouper can be used to group similar individual log messages together and count the number of number of log messages related to the issue.

Workout loads every published page on a site in an attempt to generate errors.
This can be useful for catching any module or theme errors that occur while rendering pages.

You can use workout to determine if there are any errors while loading and rendering pages. Workout will be of no help finding errors that occur when users click buttons or perform actions on pages.

The results of Workout processing runs are logged.
Workout allows you to easily compare the results of different runs.

Before a software update you can run Workout to gather base line data on each page.
After the software update you would run Workout again to gather new data on each page.
You can then view and compare the results determining if any new errors were introduced by the update.

Test Results can be downloaded to a csv file for further examination or slicing and dicing.


The following information is collected for each page that gets loaded.
  Date            The date and time the test was performed.
  Run Date        The date and time the run started.
  Nid             The node id of the node page being tested.
  Content Type    The Content Type of the node page being tested.
  Status          The html request status code for the request. 200 is perfect.
  Size            The number of bytes returned by the request. The size of the page.
  Wid Before      The last log wid (id) in the log before the test was run.
  Wid After       The last log wid (id) in the log after the test was run.
  ∆ wid           The number of log messages as a result of running the test. Should be 0.
  ∆ php           The number of php messages as a result of running the test.
  ∆ mrgnc         The number of php Emergency messages as a result of running the test.
  ∆ alert         The number of php Alert messages as a result of running the test.
  ∆ crtcl         The number of php Critical messages as a result of running the test.
  ∆ error         The number of php Error messages as a result of running the test.
  ∆ warn          The number of php Warning messages as a result of running the test.
  ∆ notice        The number of php Notice messages as a result of running the test.
  ∆ info          The number of php Info messages as a result of running the test.
  ∆ debug         The number of php Debug messages as a result of running the test.




REQUIREMENTS
------------
Drupal 8 or Drupal 9



OPTIONAL
--------
Drush 8
Drush 9



INSTALLATION
------------
Install like any other Drupal Module.



CONFIGURATION
-------------
There is no configuration for the Workout module.


OPERATION
---------
Go to the Admin Reports page and click Workout.
Use the Runs tab to create new Runs.
Choose what you want to do with the results when the testing process completes.
Click Go.

The other tabs are for displaying the Run Results.


__________________________________________________________________________________________
Runs

Admin | Reports | Workout
/admin/reports/workout/manage
Create new Runs or Delete old Runs from the Runs Page.
Use the Runs tab to create new Runs.
The other tabs are for displaying the Run Results.

__________________________________________________________________________________________
Summary

Admin | Reports | Workout | Summary
/admin/reports/workout/summary
Displays a listings of all Runs showing only pages which had errors or messages.

If no errors were detected during any of the tests there will be No Data available.
You can click Full Summary to see all the raw data from the tests.

__________________________________________________________________________________________
Full Summary

Admin | Reports | Workout | Full Summary
/admin/reports/workout/summary-full
Displays a listings of all Runs showing all pages.

The results of multiple tests are interleaved by node id so all the tests for a particular node are listed together with one another. Zebra stripes are provided for odd and even node ids to vissually group all the runs for each node together.

__________________________________________________________________________________________
CSV Summary

Admin | Reports | Workout | CSV Summary
/admin/reports/workout/summary-csv
Download a csv listings of all Runs showing all pages.

__________________________________________________________________________________________


TROUBLESHOOTING
---------------
Make sure the Workout module is enabled.
Go to Admin | Reports | Workout
/admin/reports/workout/manage


If there is a failure in the middle of a run.
You can use the drush workout-restart command
to restart an incomplete workout run.


Make sure your catching is turned off:
go to /admin/config/development/performance
Make sure cacheing is set to <no caching>
In order to catch errors when page is rendering.



FOR DRUSH ONLY

While running the workout or exerciser drush commands you may get this message.

The base_url global variable is not set for this web site.
It is set to the default url: http://default
You will need to feed in your host name as an option into exerciser or workout drush commands.
Add something like this to your drush commands:
--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-

Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.

For Drush to know the URL of your local website, you can add the following lines to your drush.local.yml file.

# File: sites/default/drush.local.yml
options:
  uri: "http://your.local.site/"

You will also need to make sure that the above drush config file is actually picked up by Drush. You can do this as follows:

# File: drush/drush.yml
drush:
  paths:
    config:
      - "web/sites/default/drush.local.yml"


# run to clear your cache
drush cr

# then run to view your sites url.
drush get-base-url

running drush uli should also now return a link to your local site instead of http://default....



FAQ
---

Q: Can I View the results of more than one Run?

A: Yes, can view the results of several runs simultaneously.

Q: How can I tell the difference between the runs?

A: The results for the same node are listed together.
   On A Summary Page the results for a node are Zebra Striped.
   Several lines forming a node group will be alternately shaded or not.
   In the drush commands there are dividing lines between the node groups.
   In a CSV file there will be a blank row between each group of nodes

Q: Can I perform the tests on another server?

A: Yes, in the Web interface add a Query String to the Runs page.
   add an option ?--host=http://the-domain-you-want-to-test
   Refresh the Runs page before starting the runoff the option to take.
   In the drush interface add a --host option to the drush command.
   workout --host=http://the-domain-you-want-to-test

Q: Can I use curl to run the tests?

A: Yes, in the Web interface add a Query String to the Runs page.
   add an option ?--curl=1
   Refresh the Runs page before starting the runoff the option to take.
   In the drush interface add a --curl option to the drush command.
   workout --curl_timeout=2

Q: Can I set the curl timeout for the tests?

A: Yes, in the Web interface add a Query String to the Runs page.
   add an option ?curl=1&curl_timeout=5
   Refresh the Runs page before starting the runoff the option to take.
   In the drush interface add a --curl_timeout=5 option go the drush command.
   workout --curl --curl_timeout=2



MAINTAINERS
-----------

Current maintainer:

 * Seth Snyder (tfa) - https://www.drupal.org/u/seth-snyder


APPENDIX - Drush Commands
-------------------------

__________________________________________________________________________________________
DRUSH COMMAND LINE INTERFACE
__________________________________________________________________________________________

workout
Performs a Workout run testing every node on the site.
If a node id is supplied only that node will be tested.

Examples:
 drush x1                                  workout                            
 drush x2                                  workout --host=http://yoursite.com 
 drush x3                                  workout --curl                     
 drush x4                                  workout --curl --curl_timeout=2

Arguments:
 nid                                       nid

Options:
 --curl                                    Use curl for connections 
 --curl_timeout                            Curl timeout in seconds. 
 --db                                      Debug                    
 --host                                    Host

Aliases: wk

__________________________________________________________________________________________

workout-restart
Restarts a workout run testing every node on the site.
You will be prompted to choose which run to restart.
The run wil resume where it ended previously.

Examples:
 drush x1                                  workout-restart                            
 drush x2                                  workout-restart --host=http://yoursite.com 
 drush x3                                  workout-restart --curl                     
 drush x4                                  workout-restart --curl --curl_timeout=2


Options:
 --curl                                    Use curl for connections 
 --curl_timeout                            Curl timeout in seconds. 
 --db                                      Debug                    
 --host                                    Host

__________________________________________________________________________________________

workout-report
Reports on previous workouts

Examples:
 drush x1                                  workout-report                     
 drush x2                                  workout-report --severity_filter=3

Arguments:
 nid                                       nid

Options:
 --all                                     Return All Rows                                                                               
 --content_type_filter                     Content Type Filter                                                                           
 --csv                                     CSV Output                                                                                    
 --severity_filter                         Severity  Filter - 
                                           |0 Emergency|1 Alert|2 Critical|3 Error|
                                           |4 Warning|5 Notice|6 Info|7 Debug| 
 --status_filter                           Status Filter

Aliases: wkr

__________________________________________________________________________________________

workout-test
Runs a workout on a specified node.

Examples:
 drush x1                                  workout-test      
 drush x2                                  workout-test 1234

Arguments:
 nid                                       nid

Options:
 --curl                                    Use curl for connections 
 --curl_timeout                            Curl timeout in seconds. 
 --db                                      Debug                    
 --host                                    Host

Aliases: wkt

__________________________________________________________________________________________

workout-history
List and Manage previous run results.

Examples:
 drush x1                                  workout-history

Aliases: wkh

__________________________________________________________________________________________

get-base-url
Displays the Drupal $base_url global variable.
Should be the domain you access drupal at.
If it is not set to anything it will return http://default.
In that case you should use the --host option to call workout or exerciser commands.

Examples:
 drush x1                                  get-base-url

Aliases: gbh

__________________________________________________________________________________________



