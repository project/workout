<?php

/**
 * @file
 * Contains workout.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

define('WORKOUT_EMERGENCY_SEVERITY', 0);
define('WORKOUT_ALERT_SEVERITY', 1);
define('WORKOUT_CRITICAL_SEVERITY', 2);
define('WORKOUT_ERROR_SEVERITY', 3);
define('WORKOUT_WARNING_SEVERITY', 4);
define('WORKOUT_NOTICE_SEVERITY', 5);
define('WORKOUT_INFO_SEVERITY', 6);
define('WORKOUT_DEBUG_SEVERITY', 7);

/**
 * Implements hook_help().
 */
function workout_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the workout module.
    case 'help.page.workout':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Accesses every node on the site.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Tests All Nodes().
 */
function workout_test() {
  $run_timestamp = time();
  \Drupal::logger('marker')->notice('MARKER - Start Workout Run - ' . date("Y/m/d H:i:s", $run_timestamp));
  $connection = \Drupal::database();
  $query = $connection->query("SELECT `nid`, `type` FROM {node_field_data} WHERE `status` = 1 ORDER BY `nid`");
  $results = $query->fetchAll();
  foreach ($results as $row) {
    workout_test_node($row->nid, $row->type, $run_timestamp);
  }
  \Drupal::logger('marker')->notice('MARKER - End Workout Run - ' . date("Y/m/d H:i:s", $run_timestamp));
}

/**
 * Tests a node().
 */

/**
 * Batch operation for testing: one at a time.
 *
 * This is the function that is called on each operation in the batch.
 * Its a batch wrapper for workout_test_node().
 */
function workout_test_node_batch_operation($nid, $content_type, $host, $curl, $curl_timeout, $db, $run_timestamp, $operation_details, &$context) {

  workout_test_node($nid, $content_type, $host, $curl, $curl_timeout, $db, $run_timestamp);

  // Store some results for post-processing in the 'finished' callback.
  // The contents of 'results' will be available as $results in the
  // A 'finished' function (in this example, batch_example_finished()).
  $context['results'][] = $nid;

  // Optional message displayed under the progressbar.
  $context['message'] = t('Running Batch "@nid" @details',
    ['@nid' => $nid, '@details' => $operation_details]
  );

}

/**
 * Batch 'finished' callback used by both batch 1 and batch 2.
 */
function workout_test_node_batch_finished($success, $results, $operations) {
  $messenger = \Drupal::messenger();
  if ($success) {
    // Here we could do something meaningful with the results.
    // We just display the number of nodes we processed...
    $messenger->addMessage(t('@count results processed.', ['@count' => count($results)]));
    // A $messenger->addMessage(t('The final result was "%final"', ['%final' => end($results)]));.
    $run_timestamp = (isset($operations['workout_test_node_batch_operation'][6]) ? $operations['workout_test_node_batch_operation'][6] : time());
    \Drupal::logger('marker')->notice('MARKER - End of Workout or Exerciser Run - ' . date("Y/m/d H:i:s", $run_timestamp));

    $tempstore = \Drupal::service('user.private_tempstore')->get('workout');
    $redirect_to_page = $tempstore->get('destination');
    if (strcmp($redirect_to_page, 'none') != 0) {
      // Redirect to report page.
      $response = new RedirectResponse('/admin/reports/workout/summary');
      $response->send();
    }

  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $messenger->addMessage(
      t('An error occurred while processing @operation with arguments : @args',
        [
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0], TRUE),
        ]
      )
    );
  }
}

/**
 * Tests a node().
 */
function workout_test_node($nid, $content_type, $host, $curl = FALSE, $curl_timeout = 0, $db = FALSE, $run_timestamp = 0) {
  $content_length = 0;
  $html_status_code = 0;
  $connection = \Drupal::database();

  $url = $host . '/node/' . $nid . '/';

  // Get the max(wid) before the test.
  $wid_before_test = $connection->query("SELECT max(`wid`) FROM {watchdog}")->fetchField();

  if ($db == FALSE) {
    // Perform the test.
    if ($curl) {
      workout_curl_get_contents($url, $content_length, $html_status_code, $curl_timeout);
    }
    else {
      workout_file_get_contents($url, $content_length, $html_status_code);
    }
  }
  else {
    // db.
    $content_length = 1000000;
    $html_status_code = 600;
  }

  // Get the max(wid) after the test.
  $wid_after_test = $connection->query("SELECT max(`wid`) FROM {watchdog}")->fetchField();

  $fields = [
    'run' => $run_timestamp,
    'nid' => $nid,
    'html_status' => $html_status_code,
    'content_size' => $content_length,
    'wid_before_test' => $wid_before_test,
    'wid_after_test' => $wid_after_test,
    'timestamp' => time(),
    'type' => $content_type,
  ];

  // See if any antries were mde in the watchdog table.
  if ($wid_after_test > $wid_before_test) {
    $fields_delta = [
      'delta_watchdog' => ($wid_after_test - $wid_before_test),
      'delta_php' => workout_get_delta_type($connection, $wid_before_test, $wid_after_test, 'php'),
      'delta_emergency' => workout_get_delta_severity($connection, $wid_before_test, $wid_after_test, WORKOUT_EMERGENCY_SEVERITY),
      'delta_alert' => workout_get_delta_severity($connection, $wid_before_test, $wid_after_test, WORKOUT_ALERT_SEVERITY),
      'delta_critical' => workout_get_delta_severity($connection, $wid_before_test, $wid_after_test, WORKOUT_CRITICAL_SEVERITY),
      'delta_error' => workout_get_delta_severity($connection, $wid_before_test, $wid_after_test, WORKOUT_ERROR_SEVERITY),
      'delta_warning' => workout_get_delta_severity($connection, $wid_before_test, $wid_after_test, WORKOUT_WARNING_SEVERITY),
      'delta_notice' => workout_get_delta_severity($connection, $wid_before_test, $wid_after_test, WORKOUT_NOTICE_SEVERITY),
      'delta_info' => workout_get_delta_severity($connection, $wid_before_test, $wid_after_test, WORKOUT_INFO_SEVERITY),
      'delta_debug' => workout_get_delta_severity($connection, $wid_before_test, $wid_after_test, WORKOUT_DEBUG_SEVERITY),
    ];

    $fields = array_merge($fields, $fields_delta);
  }

  if ($run_timestamp != 0) {
    $connection->insert('workout')
      ->fields($fields)
      ->execute();
  }

  return $fields;
}

/**
 * Check the url using file_get_contents().
 */
function workout_file_get_contents($url, &$content_length, &$html_status_code) {
  // Perform the test.
  $page = @file_get_contents($url);
  if ($page === FALSE) {
    $headers = @get_headers($url);
    if ($headers === FALSE) {
      // Error getting header.
      $content_length = 1000000;
      $html_status_code = 600;
    }
    else {
      $html_status_code = substr($headers[0], 9, 3);
      $content_length = 0;
    }
  }
  else {
    $content_length = strlen($page);
    $html_status_code = 200;
  }

}

/**
 * Check the url using curl.
 */
function workout_curl_get_contents($url, &$content_length, &$html_status_code, $timeout) {

  $url = str_replace("&amp;", "&", urldecode(trim($url)));

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
  curl_setopt($ch, CURLOPT_ENCODING, "");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
  $content = curl_exec($ch);
  $response = curl_getinfo($ch);
  curl_close($ch);

  $content_length = strlen($content);
  $html_status_code = $response['http_code'];

  return $content;
}

/**
 * Get a delta severity.
 */
function workout_get_delta_severity($connection, $wid_before_test, $wid_after_test, $severity = 0) {

  return $connection->query("SELECT count(`wid`) FROM {watchdog}
                            WHERE `wid` > :wid_before_test
                            AND `wid` <= :wid_after_test
                            AND `severity` = :severity",
                            [
                              ':wid_before_test' => $wid_before_test,
                              ':wid_after_test' => $wid_after_test,
                              ':severity' => $severity,
                            ]
                            )->fetchField();
}

/**
 * Get a delta severity.
 */
function workout_get_delta_type($connection, $wid_before_test, $wid_after_test, $type = 'php') {

  return $connection->query("SELECT count(`wid`) FROM {watchdog}
                            WHERE `wid` > :wid_before_test
                            AND `wid` <= :wid_after_test
                            AND `type` = :type",
                            [
                              ':wid_before_test' => $wid_before_test,
                              ':wid_after_test' => $wid_after_test,
                              ':type' => $type,
                            ]
                            )->fetchField();
}

/**
 * Get a list of runs.
 */
function workout_get_runs_array() {
  $output_array = [];
  $connection = \Drupal::database();
  $query = $connection->query("SELECT DISTINCT `run` FROM {workout} ORDER BY `run` ASC");
  $results = $query->fetchAll();
  foreach ($results as $row) {
    $output_array[$row->run] = ['run_date' => date('Y/m/d H:i:s', $row->run)];
  }

  return $output_array;

}

/**
 * Delete Runs.
 */
function workout_delete_runs($runs_to_delete) {
  $connection = \Drupal::database();
  // Drupals query Substitution does not work with IN.
  $query = $connection->query("DELETE FROM {workout} WHERE `run` IN ($runs_to_delete)");
  \Drupal::messenger()->addMessage('The Selected Runs Were Deleted.', 'warning');
}

/**
 * Get CSV Report.
 */
function workout_get_csv_report() {
  $hide_node = TRUE;
  $buffer = '';
  $row_nid = 0;

  // Table Header.
  $buffer = "Id,Run Date,Nid,Content Type,Status,Size,Wid Before,Wid After,∆ wid,∆ php,∆mrgnc,∆alert,∆crtcl,∆error,∆warn,∆notic,∆ info,∆debug,Time Stamp\r\n";

  /*
  // Table Header.
  $buffer = 'Id',
  'Run Date',
  'Nid',
  'Content Type',
  'Status',
  'Size',
  'Wid Before',
  'Wid After',
  '∆ wid ',
  '∆ php ',
  '∆mrgnc',
  '∆alert',
  '∆crtcl',
  '∆error',
  '∆warn ',
  '∆notic',
  '∆ info',
  '∆debug',
  'Time Stamp' . "\r\n";
   */

  $connection = \Drupal::database();
  $query = $connection->query("SELECT * FROM {workout} ORDER BY `nid` ASC, `run` ASC");
  $results = $query->fetchAll();
  foreach ($results as $row) {

    // If this is a different node.
    if ($row->nid != $row_nid) {
      $row_nid = $row->nid;
      $buffer .= "\r\n";
    }

    // CSV output.
    $buffer .=
    $row->id . ',' .
    date('Y/m/d H:i:s', $row->run) . ',' .
    $row->nid . ',' .
    $row->type . ',' .
    $row->html_status . ',' .
    $row->content_size . ',' .
    $row->wid_before_test . ',' .
    $row->wid_after_test . ',' .
    $row->delta_watchdog . ',' .
    $row->delta_php . ',' .
    $row->delta_emergency . ',' .
    $row->delta_alert . ',' .
    $row->delta_critical . ',' .
    $row->delta_error . ',' .
    $row->delta_warning . ',' .
    $row->delta_notice . ',' .
    $row->delta_info . ',' .
    $row->delta_debug . ',' .
    date('Y/m/d H:i:s', $row->timestamp) . "\r\n";

  } // OF LOOP

  return $buffer;
}
