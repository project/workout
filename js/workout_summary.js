/**
 * @file
 * workout_summary.js
 */

(function ($, Drupal) {
  var initialized

  function init () {
    if (!initialized) {
      initialized = true;

      $('a.id-link').click(function () {
        const widBefore = $(this).attr('wid-before');
        const widAfter = $(this).attr('wid-after');
        const incrementBy = 1;

        for (wid = eval(widBefore) + eval(incrementBy); wid <= widAfter; wid++) {
          window.open('/admin/reports/dblog/event/' + wid, '_blank');
        }

      })

    }
  }

  Drupal.behaviors.workout = {
    attach: function () {
      init();
    }
  }


}(jQuery, Drupal))
