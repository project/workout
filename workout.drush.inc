<?php

/**
 * @file
 * Drush for the workout module.
 */

/**
 * Implements hook_drush_command().
 */
function workout_drush_command() {

  // experimental.
  $commands['workout'] = [
    'description' => 'Runs a workout on on all nodes.',
    'aliases' => ['wk'],
    'arguments' => [
      'nid' => 'nid',
    ],
    'options' => [
      'host' => 'Host',
      'curl' => 'Use curl for connections',
      'curl_timeout' => 'Curl timeout in seconds.',
      'db' => 'Debug',
    ],
    'examples' => [
      'drush x1' => 'workout',
      'drush x2' => 'workout --host=http://yoursite.com',
      'drush x3' => 'workout --curl',
      'drush x4' => 'workout --curl --curl_timeout=2',
    ],
  ];

  $commands['workout-restart'] = [
    'description' => 'Restarts a workout run on all node.',
    'aliases' => ['wkrs'],
    'arguments' => [
      'nid' => 'nid',
    ],
    'options' => [
      'host' => 'Host',
      'curl' => 'Use curl for connections',
      'curl_timeout' => 'Curl timeout in seconds.',
      'db' => 'Debug',
    ],
    'examples' => [
      'drush x1' => 'workout-restart',
      'drush x2' => 'workout-restart --host=http://yoursite.com',
      'drush x3' => 'workout-restart --curl',
      'drush x4' => 'workout-restart --curl --curl_timeout=2',
    ],
  ];

  $commands['workout-report'] = [
    'description' => 'Reports on previous workouts',
    'aliases' => ['wkr'],
    'options' => [
      'all' => 'Return All Rows',
      'csv' => 'CSV Output',
      'content_type_filter' => 'Content Type Filter',
      'status_filter' => 'Status Filter',
      'severity_filter' => 'Severity  Filter - |0 Emergency|1 Alert|2 Critical|3 Error|4 Warning|5 Notice|6 Info|7 Debug|',
    ],
    'examples' => [
      'drush x1' => 'workout-report',
      'drush x2' => 'workout-report --csv',
      'drush x2' => 'workout-report --content_type_filter=article',
      'drush x2' => 'workout-report --status_filter=301',
      'drush x2' => 'workout-report --severity_filter=3',
    ],
  ];

  $commands['workout-test'] = [
    'description' => 'Runs a workout on a specified node.',
    'aliases' => ['wkt'],
    'arguments' => [
      'nid' => 'nid',
    ],
    'options' => [
      'host' => 'Host',
      'curl' => 'Use curl for connections',
      'curl_timeout' => 'Curl timeout in seconds.',
      'db' => 'Debug',
    ],
    'examples' => [
      'drush x1' => 'workout-test',
      'drush x2' => 'workout-test 1234',
    ],
  ];

  $commands['workout-history'] = [
    'description' => 'List and Manage previous run results.',
    'options' => [
      'all' => 'Delete All Runs',
    ],
    'aliases' => ['wkh'],
    'examples' => [
      'drush x1' => 'workout-history',
    ],
  ];

  $commands['get-base-url'] = [
    'description' => 'Gets the $base_url of thia drupal site this includes the schema.',
    'aliases' => ['gbu'],
    'examples' => [
      'drush x1' => 'get-base-url',
    ],
  ];

  return $commands;
}

/**
 * Tests and displys results.
 *
 * All or one specified node.
 */
function drush_workout($nid = NULL) {

  global $base_url;

  if (str_contains($base_url, '//default') && empty(drush_get_option('host', ''))) {
    print "The base_url global variable is not set for this web site.\r\n";
    print "It is set to the default url: $base_url.\r\n";
    print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
    print "Add something like this to your drush commands:\r\n";
    print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
    print "\r\n";
    print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
    print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
    return;
  }

  $run_timestamp = time();

  \Drupal::logger('marker')->notice('MARKER - Start Workout Run - ' . date("Y/m/d H:i:s", $run_timestamp));

  $host = drush_get_option('host', $base_url);
  $curl = drush_get_option('curl', FALSE);
  $curl_timeout_raw = drush_get_option('curl_timeout', 0);
  $curl_timeout = (isset($curl_timeout_raw) && $curl_timeout_raw != 0 ? $curl_timeout_raw : 2);
  $db = drush_get_option('db', FALSE);

  if ($nid == NULL) {
    _emit_divider_line();
    _emit_header_line();
    _emit_divider_line();

    $node_count = 1;
    $connection = \Drupal::database();
    $query = $connection->query("SELECT `nid`, `type` FROM {node_field_data} WHERE `status` = 1 ORDER BY `nid` ASC");
    $results = $query->fetchAll();
    foreach ($results as $row) {
      $fields_array = workout_test_node($row->nid, $row->type, $host, $curl, $curl_timeout, $db, $run_timestamp);
      $fields_array['id'] = $node_count;
      _emit_data_line($fields_array);
      $node_count++;
    }
    _emit_divider_line();

  }

  print "\r\n\r\nCompleted - drush workout $nid.\r\n\r\n";
  \Drupal::logger('marker')->notice('MARKER - End Workout Run - ' . date("Y/m/d H:i:s", $run_timestamp));
}

/**
 * Restart Tests and displys results.
 *
 * All or one specified node.
 */
function drush_workout_restart($nid = NULL) {

  global $base_url;

  if (str_contains($base_url, '//default') && empty(drush_get_option('host', ''))) {
    print "The base_url global variable is not set for this web site.\r\n";
    print "It is set to the default url: $base_url.\r\n";
    print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
    print "Add something like this to your drush commands:\r\n";
    print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
    print "\r\n";
    print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
    print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
    return;
  }

  $run_timestamp = _drush_choose_run('To Restart');

  $host = drush_get_option('host', $base_url);
  $curl = drush_get_option('curl', FALSE);
  $curl_timeout_raw = drush_get_option('curl_timeout', 0);
  $curl_timeout = (isset($curl_timeout_raw) && $curl_timeout_raw != 0 ? $curl_timeout_raw : 2);
  $db = drush_get_option('db', FALSE);

  if ($run_timestamp == 0) {
    print "\r\n\r\n Canceled - drush workout-restart $nid.\r\n\r\n";
    return;
  }

  \Drupal::logger('workout')->notice('Restart Run.');
  $connection = \Drupal::database();

  if ($nid == NULL) {
    // Find the max(nid) for the run and start there.
    $nid = $connection->query("SELECT MAX(`nid`) FROM {workout} WHERE `run` = :run",
                              [
                                ':run' => $run_timestamp,

                              ])->fetchField();

  }

  if (is_numeric($nid)) {
    _emit_divider_line();
    _emit_header_line();
    _emit_divider_line();

    $query = $connection->query("SELECT `nid`, `type` FROM {node_field_data}  WHERE  `status` = 1 AND `nid` >= :nid ORDER BY `nid` ASC",
                               [
                                 ':nid' => $nid,
                               ]);
    $results = $query->fetchAll();
    foreach ($results as $row) {

      _emit_data_line(workout_test_node($row->nid, $row->type, $host, $curl, $curl_timeout, $db, $run_timestamp));
    }

    _emit_divider_line();

  }

  print "\r\n\r\n Completed - drush workout-restart $nid.\r\n\r\n";
  \Drupal::logger('marker')->notice('MARKER - End Workout Run - ' . date("Y/m/d H:i:s", $run_timestamp));
}

/**
 * Displays results only.
 *
 * Filters out nodes with no errors.
 */
function drush_workout_report() {
  // If csv report requeted.
  if (drush_get_option('csv', FALSE)) {
    print workout_get_csv_repoallrt();
    return;
  }

  $node_count = 0;
  $display_count = 0;
  $hide_node = TRUE;
  // Table Header.
  _emit_divider_line();
  _emit_header_line();

  $buffer = '';
  $row_nid = 0;
  $connection = \Drupal::database();
  $query = $connection->query("SELECT * FROM {workout} ORDER BY `nid` ASC, `run` ASC");
  $results = $query->fetchAll();
  foreach ($results as $row) {

    // If this is a different node.
    if ($row->nid != $row_nid) {
      $row_nid = $row->nid;
      if ($hide_node == FALSE || drush_get_option('all', FALSE)) {
        _emit_divider_line();
        print $buffer;
        $display_count++;
      }

      $buffer = '';
      $hide_node = TRUE;
      $node_count++;
    }

    // Text output.
    $buffer .= '| ' .
    str_pad($row->id, 6) . '| ' .
    str_pad(date('Y/m/d H:i:s', $row->run), 20) . '| ' .
    str_pad($row->nid, 15) . '| ' .
    str_pad(substr($row->type, 0, 14), 15) . '| ' .
    str_pad($row->html_status, 6) . '| ' .
    str_pad($row->content_size, 8) . '| ' .
    str_pad($row->wid_before_test, 12) . '| ' .
    str_pad($row->wid_after_test, 12) . '| ' .
    str_pad($row->delta_watchdog, 6) . '| ' .
    str_pad($row->delta_php, 6) . '| ' .
    str_pad($row->delta_emergency, 6) . '| ' .
    str_pad($row->delta_alert, 6) . '| ' .
    str_pad($row->delta_critical, 6) . '| ' .
    str_pad($row->delta_error, 6) . '| ' .
    str_pad($row->delta_warning, 6) . '| ' .
    str_pad($row->delta_notice, 6) . '| ' .
    str_pad($row->delta_info, 6) . '| ' .
    str_pad($row->delta_debug, 6) . '| ' .
    str_pad(date('Y/m/d H:i:s', $row->timestamp), 20) . "|\r\n";

    if ($row->wid_before_test != $row->wid_after_test) {
      $hide_node = FALSE;
    }

  }

  // Table Footer.
  _emit_divider_line();

  if ($display_count == $node_count) {
    print "\r\nUnfiltered  - $node_count Nodes.\r\n";
  }
  else {
    print "\r\nFiltered  - $display_count Of $node_count Nodes.\r\n";
    print "\r\nFor an Unfiltered report add `--all` to the drush command.\r\n";
  }
  print "\r\nCompleted - workout-report.\r\n\r\n";
}

/**
 * Tests only.
 *
 * One specified node.
 */
function drush_workout_test($arg = NULL) {

  global $base_url;

  if (str_contains($base_url, '//default') && empty(drush_get_option('host', ''))) {
    print "The base_url global variable is not set for this web site.\r\n";
    print "It is set to the default url: $base_url.\r\n";
    print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
    print "Add something like this to your drush commands:\r\n";
    print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
    print "\r\n";
    print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
    print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
    return;
  }

  if ($arg == NULL) {
    print "\r\n\r\n You must specify either a url or node id to test.\r\n\r\n";
  }
  elseif (is_numeric($arg)) {
    $host = drush_get_option('host', $base_url);
    $curl = drush_get_option('curl', FALSE);
    $curl_timeout = 2;
    $db = drush_get_option('db', FALSE);
    _emit_divider_line();
    _emit_header_line();
    _emit_divider_line();
    _emit_data_line(workout_test_node($arg, 'type_unknown', $host, $curl, $curl_timeout, $db, time()));
    _emit_divider_line();

    print "\r\n\r\n Completed - drush workout-test $nid. \r\n\r\n";
  }
  elseif (!empty($arg)) {
    $content_length = 0;
    $html_status_code = 0;

    if (drush_get_option('curl', NULL, TRUE)) {
      workout_curl_get_contents($arg, $content_length, $html_status_code);
    }
    else {
      workout_file_get_contents($arg, $content_length, $html_status_code);
    }

    print "\r\n\r\n";
    _emit_divider_line();
    print "| URL:\t\t\t $arg\r\n";
    print "| CONTENT_LENGTH\t $content_length\r\n";
    print "| HTML_STATUS\t\t $html_status_code\r\n";
    _emit_divider_line();

    print "\r\nCompleted - workout-test $arg.\r\n\r\n";
  }

}

/**
 * Manage previous test results.
 *
 * All or one.
 */
function drush_workout_history() {

  if (drush_get_option('all', FALSE)) {

    if (drush_confirm('Are you sure you want to delete all history ?')) {
      \Drupal::database()->truncate('workout')->execute();
    }
    else {
      print "\r\n\r\n Canceled - drush workout history..\r\n\r\n";
      return;
    }

    print "\r\n\r\n Completed - drush workout history.\r\n\r\n";
  }
  else {
    // Choose a Run to delete.
    $run_timestamp = _drush_choose_run('To Delete');

    $connection = \Drupal::database();
    $query = $connection->query("DELETE FROM {workout} WHERE `run` = :run",
      [
        ':run' => $run_timestamp,

      ]);
  }
}

/**
 * Gets the $base_url of thia drupal site this includes the schema.
 */
function drush_workout_get_base_url() {
  global $base_url;
  print "\r\n$base_url\r\n\r\n";
  if (str_contains($base_url, '//default')) {
    print "The base_url global variable is not set for this web site.\r\n";
    print "It is set to the default url: $base_url.\r\n";
    print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
    print "Add something like this to your drush commands:\r\n";
    print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
    print "\r\n";
    print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
    print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
  }

}

/**
 * Helpers.
 */

/**
 * Displays Divider Line.
 */
function _emit_divider_line() {
  print '+' . str_pad('-', 211, '-') . "+\r\n";
}

/**
 * Displays Header Line.
 */
function _emit_header_line() {

  // TEXT output.
  print '| ' .
    str_pad('Id', 6) . '| ' .
    str_pad('Run Date', 20) . '| ' .
    str_pad('Nid', 15) . '| ' .
    str_pad('Content Type', 15) . '| ' .
    str_pad('Status', 6) . '| ' .
    str_pad('Size', 8) . '| ' .
    str_pad('Wid Before', 12) . '| ' .
    str_pad('Wid After', 12) . '| ' .
    str_pad('∆ wid ', 6) . '| ' .
    str_pad('∆ php ', 6) . '| ' .
    str_pad('∆mrgnc', 6) . '| ' .
    str_pad('∆alert', 6) . '| ' .
    str_pad('∆crtcl', 6) . '| ' .
    str_pad('∆error', 6) . '| ' .
    str_pad('∆warn ', 6) . '| ' .
    str_pad('∆notic', 6) . '| ' .
    str_pad('∆ info', 6) . '| ' .
    str_pad('∆debug', 6) . '| ' .
    str_pad('Time Stamp', 20) . "|\r\n";

}

/**
 * Displays Data Line.
 */
function _emit_data_line($field_array) {

  print '| ' .
    str_pad($field_array['id'], 6) . '| ' .
    str_pad(date('Y/m/d H:i:s', $field_array['run']), 20) . '| ' .
    str_pad($field_array['nid'], 15) . '| ' .
    str_pad(substr($row->type, 0, 14), 15) . '| ' .
    str_pad($field_array['html_status'], 6) . '| ' .
    str_pad($field_array['content_size'], 8) . '| ' .
    str_pad($field_array['wid_before_test'], 12) . '| ' .
    str_pad($field_array['wid_after_test'], 12) . '| ' .
    str_pad($field_array['delta_watchdog'], 6) . '| ' .
    str_pad($field_array['delta_php'], 6) . '| ' .
    str_pad($field_array['delta_emergency'], 6) . '| ' .
    str_pad($field_array['delta_alert'], 6) . '| ' .
    str_pad($field_array['delta_critical'], 6) . '| ' .
    str_pad($field_array['delta_error'], 6) . '| ' .
    str_pad($field_array['delta_warning'], 6) . '| ' .
    str_pad($field_array['delta_notice'], 6) . '| ' .
    str_pad($field_array['delta_info'], 6) . '| ' .
    str_pad($field_array['delta_debug'], 6) . '| ' .
    str_pad(date('Y/m/d H:i:s', $field_array['timestamp']), 20) . "|\r\n";

}

/**
 * Choose Run.
 */
function _drush_choose_run($why = '') {
  $options_arr = [];
  $connection = \Drupal::database();
  $query = $connection->query("SELECT DISTINCT `run` FROM {workout} ORDER BY `run` ASC");
  $results = $query->fetchAll();
  foreach ($results as $row) {
    $options_arr[$row->run] = date('Y/m/d H:i:s', $row->run);
  }
  $choice = drush_choice($options_arr, "Choose a Run $why.");
  if ($choice == 0) {
    return FALSE;
  }
  else {
    return $choice;
  }
}

/**
 * Choose Content Type.
 */
function temp_drush_choose_content_type($why = '') {
  $options_arr = [];
  $option_number = 1;
  $connection = \Drupal::database();
  $query = $connection->query("SELECT DISTINCT `type` FROM {node} ORDER BY `type` ASC");
  $results = $query->fetchAll();
  foreach ($results as $row) {
    $options_arr[$option_number++] = $row->type;
  }
  $choice = drush_choice($options_arr, "Choose content_type $why.");
  if ($choice == 0) {
    return FALSE;
  }
  else {
    return $options_arr[$choice];
  }
}

/**
 * Get Link Contents.
 */
function get_link_contents($url, &$content_length, &$html_status_code) {

  $url = str_replace("&amp;", "&", urldecode(trim($url)));

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
  curl_setopt($ch, CURLOPT_ENCODING, "");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
  $content = curl_exec($ch);
  $response = curl_getinfo($ch);
  curl_close($ch);

  $content_length = strlen($content);
  $html_status_code = $response['http_code'];

  return $content;

}
